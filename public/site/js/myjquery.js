$(".errors-login").delay(5000).slideUp();
$("body").on('change','.product_qtv',function (e) {
    e.preventDefault();
    var sl=$(this).val();
    var id=$(this).attr("product_id");

   $.ajax({
        url:"cap-nhat-gio-hang/"+id+"/"+sl,
       type:"GET",
       dataType:"json"
   }).done(function (data) {
    if(data.success){
        location.reload();
    }
   }).fail(function (erro) {
       console.log(erro);
   });
});

$("body").on("click","#payment_method_cheque",function () {
   $(".payment_method_cheque").css("display","block");
});

$("#dangky_form").validate({
   rules:{
       txt_name:"required",
       txt_email:{
           "required":true,
           "email":true
       },
       txt_password:"required"
   },
    messages:{
        txt_name:"Tên không được để trống",
        txt_email:{
            "required":"Email không được để trống",
            "email":"Email không đúng định dạng"
        },
        txt_password:"Mật khẩu không được để trống"
    },
    submitHandler:function (form) {
        form.submit();
    }
});

$("#dangnhap_form").validate({
    rules:{
        txt_name:"required",
        txt_password:"required"
    },
    messages:{
        txt_name:"Tên không được để trống",
        txt_password:"Mật khẩu không được để trống"
    },
    submitHandler:function (form) {
        form.submit();
    }
});

$("#thongtin_form").validate({
    rules:{
        name:"required",
        gender:"required",
        adress:"required",
        phone:"required",
        email:{
            "required":true,
            "email":true
        },
        txt_password:"required"
    },
    messages:{
        name:"Tên không được để trống",
        gender:"Giới tính không được để trống",
        adress:"Địa chỉ không được để trống",
        phone:"Số điện thoại không được để trống",
        email:{
            "required":"Email không được để trống",
            "email":"Email không đúng định dạng"
        },
        txt_password:"Mật khẩu không được để trống"
    },
    submitHandler:function (form) {
        form.submit();
    }
});