<?php

namespace App\Http\Controllers;

use App\Bill;
use App\BillDetail;
use App\Brand;
use App\Cart;
use App\Customer;
use App\Products;
use App\Slider;
use App\type_product;
use App\User;
use Illuminate\Http\Request;
use Session;
use Hash;
use Illuminate\Support\Facades\Auth;
use DB;
use Mail;

class PageController extends Controller
{

    public function getIndex(){
        $new_product=Products::where('new',1)->where('status',1)->limit(4)->get();
        $sale_product=Products::where('promotion_price',">",0)->where('status',1)->limit(4)->get();
        $product=Products::where('status',1)->limit(4)->get();
        $topsale_left=Products::where('promotion_price','>',0)->inRandomOrder()->limit(1)->get();
        $topsale_right=Products::where('promotion_price','>',0)->inRandomOrder()->limit(1)->get();
        return view("site.page.trangchu",compact('new_product','sale_product','product','topsale_left','topsale_right'));
    }
    public function getTypeSp($id){
    
        $loaisp=Products::where('product.id_type',$id)->get();
        $loai=type_product::all();
        $pro_hot=Products::orderBy('buy_count','desc')->take('5')->get();
        return view ('site.page.loai_san_pham',compact('loaisp','loai','pro_hot'));
    }
    public function getDetailSp(Request $req){
        $detail=Products::where('id',$req->id)->first();
        $loai=type_product::all();
        $spsame=Products::where('id_type',$detail->id_type)->inRandomOrder()->take(3)->get();
        $pro_hot=Products::orderBy('buy_count','desc')->take('5')->get();
//        dd($data_w);
        return view ('site.page.chi_tiet_san_pham',compact('detail','spsame','loai','pro_hot'));
    }
public function getCart(){
        return view('site.page.giohang');
}

    public function getAddCart(Request $req,$id){
        $product=Products::find($id);
        $oldCart=Session('cart')?Session::get('cart'):null;
        $cart=new Cart($oldCart);
        $cart->add($product,$id);
        $req->Session()->put('cart',$cart);
        return redirect()->back();
    }

    public function getUpdatecart(Request $request)
    {

        $product=Products::find($request->id);
        $oldCart=Session('cart')?Session::get('cart'):null;
        $cart=new Cart($oldCart);
        $cart->changeQtv($product,$request->id,$request->sl);
        $request->Session()->put('cart',$cart);
        return response()->json([
           'success'=>'ok',
        ]);
    }

//    xóa 1 sp trong giỏ hàng
    public function getDelCartOne($id){
        $product=Products::find($id);
        $oldCart=Session::has('cart')?Session::get('cart'):null;
        $cart=new Cart($oldCart);
        $cart->reduceByOne($product,$id);
        if(count($cart->items)>0){
            Session::put('cart',$cart);
        }
        else{
            Session::forget('cart');
        }
        return redirect()->back();
    }
//    xóa giỏ hàng
    public function getDelCart($id){
        $oldCart=Session::has('cart')?Session::get('cart'):null;
        $cart=new Cart($oldCart);
        $cart->removeItem($id);
        if(count($cart->items)>0){
            Session::put('cart',$cart);
        }
        else{
            Session::forget('cart');
        }
        return redirect()->back();
    }


    public function getContact(){

        return view ('site.page.lienhe');
    }

    public function postContact(Request $request){
        $data=[
          'email'=>$request->email,
          'subject'=>$request->subject,
          'content'=>$request->message
        ];
        Mail::send('mail',$data,function ($message) use ($data){
            $message->from($data['email']);
            $message->to('truongmanh93cntt@gmail.com')->subject($data['subject']);

        });
        return redirect()->back()->with(['mailmessage'=>'Bạn đã gửi mail thành công']);
    }


    public function getdangky(){
        return view('site.page.dangky');
    }

    public function postdangky(Request $request){

            $user=new User();
            $user->name=$request->txt_name;
            $user->email=$request->txt_email;
            $user->password=Hash::make($request->txt_password);
            $user->role=2;
            $user->status=1;
            $user->save();
            return redirect()->back()->with('thong bao','Đăng ký tài khoản thành công');
    }


    public function getdangnhap(){
        return view('site.page.dangnhap');
    }

    public function postdangnhap(Request $request){
        $login=[
          'name'=>$request->txt_name,
          'password'=>$request->txt_password,
            'role'=>2,
            'status'=>1
        ];
        if (Auth::attempt($login))
        {
            return redirect()->route("trang-chu");
        }
        else{
            return redirect()->back()->with('thong bao','Đăng nhập thất bại,xin vui lòng nhập đúng tên hoặc mật khẩu');
        }
    }

    public function postDangxuat(){
        Auth::logout();
        return redirect()->route("trang-chu");
    }

    public function getTimKiem(Request $req){
        $product=Products::where('name','like','%'.$req->search.'%')->get();
        $loai=type_product::all();
        $pro_hot=Products::orderBy('buy_count','desc')->take('5')->get();
        return view('site.page.timkiem',compact('product','loai','pro_hot'));
    }

    public function getGioithieu(){
        return view('site.page.gioithieu');
    }


    public function getCheckout(){
        if(Auth::check()){
            $data_cuss=Customer::where('email','=',Auth::user()->email)
                ->first();
           if($data_cuss){
               $data=$data_cuss;
               return view('site.page.dathang',compact('data'));
           }
           else{
               $data=User::find(Auth::user()->id);
               return view('site.page.dathang',compact('data'));
           }
        }
        else{
            return view('site.page.dathang');
        }

    }
    public function postCheckout(Request $request){

              $cart=Session::get('cart');
             if(Auth::check()){
                 $data_cuss=Customer::where('email','=',Auth::user()->email)
                     ->first();
                 if($data_cuss){
                     $data_cuss->name=$request->name;
                     $data_cuss->gender=$request->gender;
                     $data_cuss->phone_number=$request->phone;
                     $data_cuss->address=$request->adress;
                     $data_cuss->email=$request->email;
                     $data_cuss->note=$request->note;
                     $data_cuss->update();

                     $bill=new Bill();
                     $bill->id_customer= $data_cuss->id;
                     $bill->date_order=date('Y-m-d');
                     $bill->total=$cart->totalPrice;
                     $bill->payment=$request->payment_method;
                     $bill->note=$request->note;
                     $bill->status="0";
                     $bill->save();


                     foreach ($cart->items as $key=>$value){
                         $bill_detail=new BillDetail();
                         $bill_detail->id_bill=$bill->id;
                         $bill_detail->id_product=$key;
                         $bill_detail->quantity=$value['qty'];
                         $bill_detail->unit_price=$value['price']/$value['qty'];
                         $bill_detail->save();

                         $data=Products::findOrFail($key);
                         $data->increment('buy_count');
                     }

                     Session::forget('cart');
                 }
                 else{
                     $customer=new Customer();
                     $customer->name=$request->name;
                     $customer->gender=$request->gender;
                     $customer->phone_number=$request->phone;
                     $customer->address=$request->adress;
                     $customer->email=$request->email;
                     $customer->note=$request->note;
                     $customer->save();

                     $user1=User::find(Auth::user()->id);
                     $user1->id_cus= $customer->id;
                     $user1->update();

                     $bill=new Bill();
                     $bill->id_customer= $customer->id;
                     $bill->date_order=date('Y-m-d');
                     $bill->total=$cart->totalPrice;
                     $bill->payment=$request->payment_method;
                     $bill->note=$request->note;
                     $bill->status="0";
                     $bill->save();


                     foreach ($cart->items as $key=>$value){
                         $bill_detail=new BillDetail();
                         $bill_detail->id_bill=$bill->id;
                         $bill_detail->id_product=$key;
                         $bill_detail->quantity=$value['qty'];
                         $bill_detail->unit_price=$value['price']/$value['qty'];
                         $bill_detail->save();

                         $data=Products::findOrFail($key);
                         $data->increment('buy_count');
                     }

                     Session::forget('cart');
                 }

             }
             else{
                 $data_cus=Customer::where('email','=',$request->email)
                     ->first();
                 if(!$data_cus){
                     $customer=new Customer();
                     $customer->name=$request->name;
                     $customer->gender=$request->gender;
                     $customer->phone_number=$request->phone;
                     $customer->address=$request->adress;
                     $customer->email=$request->email;
                     $customer->note=$request->note;
                     $customer->save();

                     $user1=new User();
                     $user1->name=$request->name;
                     $user1->email=$request->email;
                     $user1->password=Hash::make(123456);
                     $user1->role=2;
                     $user1->status=1;
                     $user1->id_cus= $customer->id;
                     $user1->save();

                     $bill=new Bill();
                     $bill->id_customer=$customer->id;
                     $bill->date_order=date('Y-m-d');
                     $bill->total=$cart->totalPrice;
                     $bill->payment=$request->payment_method;
                     $bill->note=$request->note;
                     $bill->status="0";
                     $bill->save();


                     foreach ($cart->items as $key=>$value){
                         $bill_detail=new BillDetail();
                         $bill_detail->id_bill=$bill->id;
                         $bill_detail->id_product=$key;
                         $bill_detail->quantity=$value['qty'];
                         $bill_detail->unit_price=$value['price']/$value['qty'];
                         $bill_detail->save();

                         $data=Products::findOrFail($key);
                         $data->increment('buy_count');
                     }

                     Session::forget('cart');
                     $login=[
                         'name'=>$request->name,
                         'password'=>'123456',
                         'role'=>2,
                         'status'=>1
                     ];
                     Auth::attempt($login);
                 }
                 else{

                     $bill=new Bill();
                     $bill->id_customer=$data_cus->id;
                     $bill->date_order=date('Y-m-d');
                     $bill->total=$cart->totalPrice;
                     $bill->payment=$request->payment_method;
                     $bill->note=$request->note;
                     $bill->status="0";
                     $bill->save();


                     foreach ($cart->items as $key=>$value){
                         $bill_detail=new BillDetail();
                         $bill_detail->id_bill=$bill->id;
                         $bill_detail->id_product=$key;
                         $bill_detail->quantity=$value['qty'];
                         $bill_detail->unit_price=$value['price']/$value['qty'];
                         $bill_detail->save();

                         $data=Products::findOrFail($key);
                         $data->increment('buy_count');
                     }

                     Session::forget('cart');
                     $data_u=DB::table('users')
                         ->join('customer','customer.id','=','users.id_cus')
                         ->first();
                     $login=[
                         'name'=>$data_u->name,
                         'password'=>$data_u->password,
                         'role'=>2,
                         'status'=>1
                     ];
                     Auth::attempt($login);
                 }
             }

                return redirect()->back()->with(['thong bao'=>'Đặt hàng thành công,chúng tôi sẽ giao hàng cho bạn trong thời gian nhanh nhất']);

    }

    public function getFilterProduct(Request $req){
        $id_sx=$req->id;
        $id_type=$req->id_type;
        $id_brand=$req->id_brand;
        $am0=$req->am0;
        $am1=$req->am1;
        if($id_sx!=0&& $id_brand ==0&&$am0==300 && $am1==300){
            switch ($id_sx){
                case 7:
                    $data=Products::where('id_type',$id_type)->orderBy('name', 'asc')->get();
                    break;
                case 8:
                    $data=Products::where('id_type',$id_type)->orderBy('name', 'desc')->get();
                    break;
                case 9:
                    $data=Products::where('id_type',$id_type)->orderBy('unit_price', 'desc')->get();
                    break;

                case 10:
                    $data=Products::where('id_type',$id_type)->orderBy('unit_price', 'asc')->get();
                    break;
            }
        }
        else if($id_brand !=0 && $id_sx==0&&$am0==300 && $am1==300){
            $data=Products::where('id_type',$id_type)->where('brand_id',$id_brand)->get();
        }
        else if($id_brand ==0 && $id_sx==0&&($am0==300 && $am1!=300||$am0!=300 && $am1!=300)){
            $data=Products::where('id_type',$id_type)->whereBetween('unit_price', [$am0,$am1])->get();
        }
        else if($id_brand !=0 && $id_sx !=0&&$am0==300 && $am1==300){
            switch ($id_sx){
                case 7:
                    $data=Products::where('id_type',$id_type)->where('brand_id',$id_brand)->orderBy('name', 'asc')->get();
                    break;
                case 8:
                    $data=Products::where('id_type',$id_type)->where('brand_id',$id_brand)->orderBy('name', 'desc')->get();
                    break;
                case 9:
                    $data=Products::where('id_type',$id_type)->where('brand_id',$id_brand)->orderBy('unit_price', 'desc')->get();
                    break;

                case 10:
                    $data=Products::where('id_type',$id_type)->where('brand_id',$id_brand)->orderBy('unit_price', 'asc')->get();
                    break;
            }
        }
        else if($id_brand !=0 && $id_sx ==0&&($am0==300 && $am1!=300||$am0!=300 && $am1!=300)){
            $data=Products::where('id_type',$id_type)->where('brand_id',$id_brand)->whereBetween('unit_price', [$am0,$am1])->get();
        }
        else if($id_brand ==0 && $id_sx !=0&&($am0==300 && $am1!=300||$am0!=300 && $am1!=300)){
            switch ($id_sx){
                case 7:
                    $data=Products::where('id_type',$id_type)->whereBetween('unit_price', [$am0,$am1])->orderBy('name', 'asc')->get();
                    break;
                case 8:
                    $data=Products::where('id_type',$id_type)->whereBetween('unit_price', [$am0,$am1])->orderBy('name', 'desc')->get();
                    break;
                case 9:
                    $data=Products::where('id_type',$id_type)->whereBetween('unit_price', [$am0,$am1])->orderBy('unit_price', 'desc')->get();
                    break;

                case 10:
                    $data=Products::where('id_type',$id_type)->whereBetween('unit_price', [$am0,$am1])->orderBy('unit_price', 'asc')->get();
                    break;
            }
            }
            else if($id_brand !=0 && $id_sx !=0&&($am0==300 && $am1!=300||$am0!=300 && $am1!=300)){
                switch ($id_sx){
                    case 7:
                        $data=Products::where('id_type',$id_type)->whereBetween('unit_price', [$am0,$am1])->where('brand_id',$id_brand)->orderBy('name', 'asc')->get();
                        break;
                    case 8:
                        $data=Products::where('id_type',$id_type)->whereBetween('unit_price', [$am0,$am1])->where('brand_id',$id_brand)->orderBy('name', 'desc')->get();
                        break;
                    case 9:
                        $data=Products::where('id_type',$id_type)->whereBetween('unit_price', [$am0,$am1])->where('brand_id',$id_brand)->orderBy('unit_price', 'desc')->get();
                        break;

                    case 10:
                        $data=Products::where('id_type',$id_type)->whereBetween('unit_price', [$am0,$am1])->where('brand_id',$id_brand)->orderBy('unit_price', 'asc')->get();
                        break;
                }
            }


        return view("site.page.filterpro",compact('data'));
    }
}
