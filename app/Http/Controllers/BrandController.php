<?php

namespace App\Http\Controllers;

use App\Brand;
use Illuminate\Http\Request;

class BrandController extends Controller
{
   public function index(){

   }
   public function getListBrand(){
       $brand=Brand::paginate(4);
       return view('admin.brand.list',compact('brand'));
   }

   public function getAddBrand(){
       return view('admin.brand.add');
   }

   public function postAddBrand(Request $request){
        $brand=new Brand();
        $brand->name=$request->name;
        $brand->status=$request->status;
        $brand->save();
       return redirect()->route('getListBrand')->with(['flash_message'=>'Thêm nhãn hiệu thành công']);
   }

   public function getEditBrand($id){
       $data=Brand::find($id);
       return view('admin.brand.edit',compact('data'));
   }

   public function postEditBrand(Request $request, $id){
       $brand=Brand::find($id);
       $brand->name=$request->name;
       $brand->status=$request->status;
       $brand->update();
       return redirect()->route('getListBrand')->with(['flash_message'=>'Sửa nhãn hiệu thành công']);
   }
public function getDelBrand($id){
    $brand=Brand::find($id);
    $brand->delete();
    return redirect()->route('getListBrand')->with(['flash_message'=>'xóa nhãn hiệu thành công']);

}
}
