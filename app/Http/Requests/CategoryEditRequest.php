<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CategoryEditRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'txt_name'=>'required',
            'txt_des'=>'required',
            'txt_image'=>'image'
        ];
    }
    public function messages()
    {
        return[
           'txt_name.required'=>'Tên không được để trống',
           'txt_des.required'=>'Miêu tả không được để trống',
           'txt_image.image'=>'phải là ảnh',
        ];
    }
}
