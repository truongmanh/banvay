
<VirtualHost *:80>
       ServerName foodhy.com
       DocumentRoot "E:\xampp\htdocs\Laravel\banhang\public"

       <Directory "E:\xampp\htdocs\Laravel\banhang\public">
           # use mod_rewrite for pretty URL support
           RewriteEngine on
           # If a directory or a file exists, use the request directly
           RewriteCond %{REQUEST_FILENAME} !-f
           RewriteCond %{REQUEST_FILENAME} !-d
           # Otherwise forward the request to index.php
           RewriteRule . index.php

           # use index.php as index file
           DirectoryIndex index.php
           # ...other settings...
     Require all granted 
       </Directory>
   </VirtualHost>