@extends('admin.master')
@section('title','Danh sách slider')
@section('content')
    @if(Session::has('flash_message'))
        <div class="alert alert-danger">
            {{Session::get('flash_message')}}
        </div>
    @endif
    <section class="content-header">
        <h1>
            Quản lí slider
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>Trang chủ</a></li>
            <li class="active">Danh sách Slider</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Danh sách slider</h3>
            </div>
            <a href="{{route('getAddSlider')}}"  class="pull-right" style="margin-right:-5px"> <button class="btn btn-primary" class="pull-right" style="margin-right:15px">Thêm slider</button></a>
            <!-- /.box-header -->
            <div class="box-body">
                <table class="table table-bordered">
                    <tbody><tr>
                        <th style="width: 10px">STT</th>
                        <th>Hình ảnh</th>
                        <th>Trạng thái</th>
                        <th>Hành động</th>
                        {{--<th style="width: 40px">Label</th>--}}
                    </tr>
                    <?php $stt=1?>
                    @foreach($data as $item)
                        <tr>
                            <td>{{$stt++}}</td>
                            <td><img src="uploads/slider/{{$item->link}}" width="100px"></td>
                            <td>
                                @if($item->status==0)
                                    {{"Ẩn"}}
                                @else
                                    {{"Hiện"}}
                                @endif
                            </td>
                            <td>
                                <a href="{{route('getEditSlider',['id'=>$item->id])}}"><button type="button" class="btn btn-success btn-sm">Sửa</button></a> <a href="{{route('getDelSlider',['id'=>$item->id])}}"><button type="button" class="btn btn-danger btn-sm">Xóa</button></a></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">
                <ul class="pagination pagination-sm no-margin pull-right">
                    {{ $data->links() }}
                </ul>
            </div>
        </div>

        <!-- /.row -->
    </section>
@endsection