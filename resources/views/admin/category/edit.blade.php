@extends('admin.master')
@section('title','Sửa danh mục')
@section('content')
    @if(count($errors)>0)
        <div class="alert alert-danger">
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <section class="content-header">
        <h1>
            Quản lí danh mục
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>Trang chủ</a></li>
            <li class="active">Sửa danh mục</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Thêm danh mục</h3>
                    </div>
                    <form role="form" action="{{route('postEditCate',['id'=>$data['id']])}}" method="post" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <div class="box-body">
                            <div class="form-group">
                                <label for="txt_name">Danh mục cha</label>
                                <select class="form-control" name="slt_name">
                                    <option value="0">Root</option>
                                    <?php Menu_mutil($data_cate,0,"--|",$data['parent_id'])?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="">Tên danh mục</label>
                                <input type="text" name="txt_name" class="form-control" id="txt_name" placeholder="name" value="{{old('txt_name',isset($data)?$data['name']:null)}}">
                            </div>
                            <div class="form-group">
                                <label for="">Mô tả</label>
                                <textarea class="form-control" name="txt_des" placeholder="description">{{old('txt_des',isset($data)?$data['description']:null)}}</textarea>
                            </div>
                            <div class="form-group">
                                <label for="">Trạng thái</label>
                                <select class="form-control" name="slt_status">
                                    @if($data['status']==0)
                                        <option value="1">Hiện</option>
                                        <option value="0" selected="selected">Ẩn</option>
                                    @else
                                        <option value="1" selected="selected">Hiện</option>
                                        <option value="0">Ẩn</option>
                                    @endif
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="">Icon</label>
                                <input type="file" name="txt_img" id="txt_img" placeholder="name">
                                <input type="hidden" name="txt_img_current" value="{{$data['image']}}"><br />
                                <img src="{{asset('/uploads/admin/cate/'.$data['image'])}}" alt="" width="100px">
                            </div>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <a href="{{route('getListCate')}}"><button type="button" class="btn btn-danger pull-right " style="margin-left: 5px">Cancel</button></a>
                            <button type="submit" class="btn btn-primary pull-right">Sửa danh mục</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

@endsection