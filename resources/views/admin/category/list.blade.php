@extends('admin.master')
@section('title','Danh sách danh mục')
@section('content')
    @if(Session::has('flash_message'))
        <div class="alert alert-danger">
            {{Session::get('flash_message')}}
        </div>
        @endif
    <section class="content-header">
        <h1>
            Quản lí danh mục
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>Trang chủ</a></li>
            <li class="active">Danh sách danh mục</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Danh sách danh mục</h3>
            </div>
            <a href="{{route('getAddCate')}}"  class="pull-right" style="margin-right:-5px"> <button class="btn btn-primary" class="pull-right" style="margin-right:15px">Thêm danh mục</button></a>
            <!-- /.box-header -->
            <div class="box-body">
                <table class="table table-bordered">
                    <tbody><tr>
                        <th style="width: 10px">STT</th>
                        <th>Tên</th>
                        <th>Danh mục cha</th>
                        <th>Trạng thái</th>
                        <th>Hành động</th>
                        {{--<th style="width: 40px">Label</th>--}}
                    </tr>
                    <?php $stt=1?>
                    @foreach($data as $item)
                        <tr>
                            <td>{{$stt++}}</td>
                            <td>{{$item->name}}</td>
                            <td>{{$item->parent_id}}</td>
                            <td>
                                @if($item->status==0)
                                    {{"Ẩn"}}
                                @else
                                    {{"Hiện"}}
                                @endif
                            </td>
                            <td>
                               <a href="{{route('getEditCate',['id'=>$item->id])}}"> <button type="button" class="btn btn-success btn-sm">Sửa</button></a> <a href="{{route('getDelCate',['id'=>$item->id])}}"><button type="button" class="btn btn-danger btn-sm">Xóa</button></a></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">
                <ul class="pagination pagination-sm no-margin pull-right">
                    {{ $data->links() }}
                </ul>
            </div>
        </div>

        <!-- /.row -->
    </section>

    @endsection