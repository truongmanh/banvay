@extends('admin.master')
@section('title','Sửa sản phẩm')
@section('content')
    @if(count($errors)>0)
        <div class="alert alert-danger">
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <section class="content-header">
        <h1>
            Quản lí sản phẩm
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>Trang chủ</a></li>
            <li class="active">Sửa sản phẩm</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Sửa sản phẩm</h3>
                    </div>
                    <form role="form" action="{{route('postEditProduct',['id'=>$data['id']])}}" method="post" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <div class="box-body">
                            <div class="form-group">
                                <label for="txt_name">Tên sản phẩm</label>
                                <input type="text" class="form-control" name="txt_name" placeholder="name" value="{{old('txt_name',isset($data)?$data['name']:null)}}">
                            </div>
                            <div class="form-group">
                                <label for="">Tên danh mục</label>
                                <select class="form-control" name="slt_cate">
                                    <?php Menu_mutil($data_cate,0,"--|",$data['id_type'])?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="">Mô tả</label>
                                <textarea class="form-control" name="txt_content">{{old('txt_content',isset($data)?$data['description']:null)}}</textarea>
                                <script type="text/javascript">
                                    var editor = CKEDITOR.replace('txt_content',{
                                        language:'vi',
                                        filebrowserBrowseUrl :"{{asset('/admin/js/plugin/ckfinder/ckfinder.html')}}",
                                        filebrowserImageBrowseUrl : "{{asset('/admin/js/plugin/ckfinder/ckfinder.html?type=Images')}}",
                                        filebrowserFlashBrowseUrl :"{{asset( '/admin/js/plugin/ckfinder/ckfinder.html?type=Flash')}}",
                                        filebrowserUploadUrl : "{{asset('/admin/js/plugin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files')}}",
                                        filebrowserImageUploadUrl : "{{asset('/admin/js/plugin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images')}}",
                                        filebrowserFlashUploadUrl : "{{asset('/admin/js/plugin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash')}}",
                                    });
                                </script>﻿
                            </div>
                            <div class="form-group">
                                <label for="txt_name">Giá</label>

                                <input type="text" class="form-control" name="txt_price" placeholder="name" value="{{old('txt_price',isset($data)?$data['unit_price']:null)}}">

                            </div>
                            <div class="form-group">
                                <label for="">Giá khuyến mãi</label>
                                <input type="text" class="form-control" name="txt_promo_price" placeholder="name" value="{{old('txt_promo_price',isset($data)?$data['promotion_price']:null)}}">
                            </div>
                            <div class="form-group">
                                <label for="">Hàng mới</label>
                                <select class="form-control" name="slt_new">
                                    @if($data['new']==1)
                                        <option value="1" selected>Yes</option>
                                        <option value="0">No</option>
                                    @else
                                        <option value="1" >Yes</option>
                                        <option value="0" selected>No</option>
                                    @endif
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="">Ảnh</label>
                                <input type="file" name="txt_img" placeholder="name">
                                <input type="hidden" name="txt_img_current" value="{{$data['image']}}"><br />
                                <img src="{{asset('/uploads/product/'.$data['image'])}}" alt="" width="100px">
                            </div>
                            <div class="form-group">
                                <label for="">Trạng thái</label>
                                <select class="form-control" name="slt_status">
                                    @if($data['status']==1)
                                        <option value="1" selected>Hiện</option>
                                        <option value="0">Ẩn</option>
                                    @else
                                        <option value="1">Hiện</option>
                                        <option value="0"  selected>Ẩn</option>
                                    @endif
                                </select>
                            </div>
                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer">
                            <a href="{{route('getListProduct')}}"><button type="button" class="btn btn-danger pull-right " style="margin-left: 5px">Cancel</button></a>
                            <button type="submit" class="btn btn-primary pull-right">Sửa sản phẩm</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

@endsection