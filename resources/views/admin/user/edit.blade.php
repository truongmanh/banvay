@extends('admin.master')
@section('title','Sửa user')
@section('content')
@if(count($errors)>0)
        <div class="alert alert-danger">
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <section class="content-header">
        <h1>
            Quản lí user
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>Trang chủ</a></li>
            <li class="active">Sửa user</li>
        </ol>
    </section>
    <section class="content">
       <div class="row">
           <div class="col-md-8 col-md-offset-2">
               <div class="box box-primary">
                   <div class="box-header with-border">
                       <h3 class="box-title">Sửa user</h3>
                   </div>
                   <form role="form" action="{{route('postAddUser')}}" method="post" enctype="multipart/form-data">
                       {{csrf_field()}}
                       <div class="box-body">
                           <div class="form-group">
                                   <label for="txt_name" >Name</label>
                                    <input type="text" class="form-control" name="txt_name" value="{{user->name}}" >
                           </div>
                           <div class="form-group">
                               <label for="txt_name" >Email</label>
                               <input type="email" class="form-control" name="txt_email" value="{{user->email}}">
                          
                           </div>
                           <div class="form-group">
                           <label for="txt_name">Password</label>
                         
                               <input type="password" class="form-control" name="txt_pass" placeholder="name">
                         
                           </div>
                           <div class="form-group">
                            <label for="txt_status" >Role</label>
                           
                               <select class="form-control" name="slt_role">
                                   <option value="1">Admin</option>
                                   <option value="0">Staff</option>
                               </select>

                           </div>
                           <div class="form-group">
                           <label for="txt_status" >Status</label>
                          
                               <select class="form-control" name="slt_status">
                                   <option value="1">show</option>
                                   <option value="0">hidden</option>
                               </select>
                          
                           </div>
                       </div>
                       <!-- /.box-body -->

                       <div class="box-footer">
                           <a href="{{route('getListUser')}}"><button type="button" class="btn btn-danger pull-right " style="margin-left: 5px">Cancel</button></a>
                           <button type="submit" class="btn btn-primary pull-right">Thêm users</button>
                       </div>
                   </form>
               </div>
           </div>
       </div>
    </section>
@endsection