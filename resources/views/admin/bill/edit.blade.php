@extends('admin.master')
@section('title','Sửa đơn hàng')
@section('content')
    @if(count($errors)>0)
        <div class="alert alert-danger">
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <section class="content-header">
        <h1>
            Quản lí đơn hàng
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>Trang chủ</a></li>
            <li class="active">Sửa đơn hàng</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Sửa đơn hàng</h3>
                    </div>
                    <form role="form" action="{{route('postEditBill',['id'=>$data['id']])}}" method="post" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <div class="box-body">
                            <div class="form-group">
                                <label for="">Trạng thái</label>
                                @if($data->status ==1)
                                <select name="txtstatus"  class="form-control">
                                    <option value="0">Chưa thanh toán</option>
                                    <option value="1" selected >Đã thanh toán</option>
                                </select>
                                @else
                                    <select name="txtstatus"  class="form-control">
                                        <option value="0" selected>Chưa thanh toán</option>
                                        <option value="1" >Đã thanh toán</option>
                                    </select>
                                @endif
                            </div>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <a href="{{route('getListBill')}}"><button type="button" class="btn btn-danger pull-right " style="margin-left: 5px">Cancel</button></a>
                            <button type="submit" class="btn btn-primary pull-right">Sửa đơn hàng</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

@endsection