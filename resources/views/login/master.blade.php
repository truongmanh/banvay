<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>@yield('title')</title>
    <base href="{{ asset('') }}">
    <link rel="stylesheet" href="admin/css/bootstrap.min.css">
    <script src="admin/js/jquery-3.2.1.js" type="text/javascript" charset="utf-8"></script>
    {{--<script src="admin/js/bootstrap.min.js" type="text/javascript" charset="utf-8"></script>--}}
    <link rel="stylesheet" type="text/css" href="admin/css/style.css?v=<?=time()?>">
    <script src="assets/dest/js/jquery.validate.js"></script>
</head>

<body>

  @yield('content')

</body>
<script>


$(".errors-loginn").delay(3000).slideUp("slow")
$("#formli").validate({
//specify the validation rules
rules: {
txt_name: "required",
txt_pass: "required",
},
 
//specify validation error messages
messages: {
  txt_name: "Tên không được để trống",
  txt_pass: "mật khẩu không được để trống",
},
 
submitHandler: function(form){
form.submit();
}
 
});
</script>
</html>