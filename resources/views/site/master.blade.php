<!DOCTYPE html>
<html>
<head>
    <title>@yield('title')</title>

    <base href="{{asset('')}}">
    <!--//theme-style-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="Youth Fashion Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
    <!-- Custom Theme files -->
    <!--theme-style-->
    <link href="site/css/bootstrap-3.1.1.min.css" rel='stylesheet' type='text/css' />
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="site/js/jquery.min.js"></script>
    <link href="site/css/style.css?v=<?= time()?>" rel="stylesheet" type="text/css" media="all" />
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
    {{--<link href='//fonts.googleapis.com/css?family=Raleway' rel='stylesheet' type='text/css'>--}}
    {{--<link href='//fonts.googleapis.com/css?family=Poiret+One' rel='stylesheet' type='text/css'>--}}
    <link href='http://fonts.googleapis.com/css?family=Dosis:300,400' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300' rel='stylesheet' type='text/css'>
    <!-- start menu -->
    <script src="site/js/bootstrap.min.js"></script>
    <script src="site/js/simpleCart.min.js"> </script>
    <!-- slide -->
    <script src="site/js/responsiveslides.min.js"></script>
    <script>
        $(function () {
            $("#slider").responsiveSlides({
                auto: false,
                speed: 500,
                namespace: "callbacks",
                pager: true,
            });
        });
    </script>
    <!-- animation-effect -->
    <link href="site/css/animate.min.css" rel="stylesheet">
    <script src="site/js/wow.min.js"></script>
    <script>
        new WOW().init();
    </script>
    <!-- //animation-effect -->

</head>
<body>
<!--header-->
@include('site.header')
<!--banner-->
@include('site.banner')
<!--//banner-->
<!--content-->
@yield('content')
<!--footer-->
@include('site.footer')
<!--footer-->
{{--<script type="text/javascript">--}}
    {{--$(function() {--}}
        {{--var menu_ul = $('.menu-drop > li > ul'),--}}
            {{--menu_a  = $('.menu-drop > li > a');--}}
        {{--menu_ul.hide();--}}
        {{--menu_a.click(function(e) {--}}
            {{--e.preventDefault();--}}
            {{--if(!$(this).hasClass('active')) {--}}
                {{--menu_a.removeClass('active');--}}
                {{--menu_ul.filter(':visible').slideUp('normal');--}}
                {{--$(this).addClass('active').next().stop(true,true).slideDown('normal');--}}
            {{--} else {--}}
                {{--$(this).removeClass('active');--}}
                {{--$(this).next().stop(true,true).slideUp('normal');--}}
            {{--}--}}
        {{--});--}}

    {{--});--}}
{{--</script>--}}
<!-- FlexSlider -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.js"></script>
<script defer src="site/js/jquery.flexslider.js"></script>
<script defer src="site/js/myjquery.js?v=<?=time()?>"></script>
<script src="site/js/imagezoom.js"></script>
<link rel="stylesheet" href="site/css/flexslider.css" type="text/css" media="screen" />

<script>
    // Can also be used with $(document).ready()
    $(window).load(function() {
        $('.flexslider').flexslider({
            animation: "slide",
            controlNav: "thumbnails"
        });
    });
</script>
<!---pop-up-box---->
<link href="site/css/popuo-box.css" rel="stylesheet" type="text/css" media="all"/>
<script src="site/js/jquery.magnific-popup.js" type="text/javascript"></script>
<!---//pop-up-box---->
<script>
    $(document).ready(function() {
        $('.popup-with-zoom-anim').magnificPopup({
            type: 'inline',
            fixedContentPos: false,
            fixedBgPos: true,
            overflowY: 'auto',
            closeBtnInside: true,
            preloader: false,
            midClick: true,
            removalDelay: 300,
            mainClass: 'my-mfp-zoom-in'
        });

    });
</script>
</body>
</html>