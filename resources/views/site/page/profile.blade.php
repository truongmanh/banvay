@extends('site.master')
@section('title','cá nhân')
@section('content')
    <div class="rev-slider">

        <div class="fullwidthbanner-container">
            <div class="fullwidthbanner">
                <div class="bannercontainer" >
                    <div class="banner" >
                        <ul>
                            <!-- THE FIRST SLIDE -->
                            @foreach($silde as $value)
                            <li data-transition="boxfade" data-slotamount="20" class="active-revslide" style="width: 100%; height: 100%; overflow: hidden; z-index: 18; visibility: hidden; opacity: 0;">
                                <div class="slotholder" style="width:100%;height:100%;" data-duration="undefined" data-zoomstart="undefined" data-zoomend="undefined" data-rotationstart="undefined" data-rotationend="undefined" data-ease="undefined" data-bgpositionend="undefined" data-bgposition="undefined" data-kenburns="undefined" data-easeme="undefined" data-bgfit="undefined" data-bgfitend="undefined" data-owidth="undefined" data-oheight="undefined">
                                    <div class="tp-bgimg defaultimg" data-lazyload="undefined" data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat" data-lazydone="undefined" src="uploads/slider/{{$value->link }}" data-src="uploads/slider/{{$value->link }}" style="background-color: rgba(0, 0, 0, 0); background-repeat: no-repeat; background-image: url('uploads/slider/{{$value->link }}'); background-size: cover; background-position: center center; width: 100%; height: 100%; opacity: 1; visibility: inherit;">
                                    </div>
                                </div>
    
                            </li>
                            @endforeach

                        </ul>
                    </div>
                </div>

                <div class="tp-bannertimer"></div>
            </div>
        </div>
        <!--slider-->
    </div>
    <div class="container">
        <div id="content" class="space-top-none">
            <div class="main-content">
                <div class="space60">&nbsp;</div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="beta-products-list">

                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="profile">
                                        <h3>Thông tin cá nhân</h3>
                                           <div class="article">
                                               <p>Tài khoản:{{Auth::user()->name}}</p>
                                               <p>Tài khoản:{{Auth::user()->email}}</p>
                                               <p>Trạng thái:@if(Auth::user()->status==1){{"Hoạt động"}}@endif</p>
                                               <p>Mật khẩu mặc định nếu bạn chưa đăng ký tài khoản:123456</p>
                                           </div>
                                        <h3>Lịch sử mua hàng</h3>
                                        <table class="table table-bordered">
                                            <tbody>
                                                <tr>
                                                    <td>STT</td>
                                                    <td>Hình ảnh</td>
                                                    <td>Tên hàng</td>
                                                    <td>Giá tiền</td>
                                                    <td>Số lượng</td>
                                                    <td>Ngày mua</td>
                                                </tr>
                                                <?php $stt=1?>
                                                @foreach($data as $item)
                                                    <tr>
                                                        <td><?= $stt++?></td>
                                                        <td>
                                                            <img src="uploads/product/{{$item->image}}" alt="" width="100px">
                                                        </td>
                                                        <td>{{$item->name}}</td>
                                                        <td>{{number_format($item->unit_price,3)}}</td>
                                                        <td>{{$item->quantity}}</td>
                                                        <td>{{$item->date_order}}</td>
                                                    </tr>
                                                    @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="row">
                                   @if(!empty($data)){
                                        {{$data->links()}}
                                    }
                                       @endif
                                </div>
                            </div>
                        </div> <!-- .beta-products-list -->

                        <div class="space50">&nbsp;</div>

                        <div class="beta-products-list">
                            <h4>Sản phẩm HOT</h4>
                            <div class="beta-products-details">
                                {{--<p class="pull-left">Tim thay{{count($sale_product)}}</p>--}}
                                <div class="clearfix"></div>
                            </div>

                            <div class="row">
                                @foreach($hot_product as $spkm)
                                    <div class="col-sm-3">
                                        <div class="single-item">
                                            <div class="single-item-header">
                                                <a href="{{route('chitietsanpham',[$spkm->id,$spkm->alias])}}"><img src="uploads/product/{{$spkm->image}}" height="250px" alt=""></a>
                                            </div>
                                            <div class="single-item-body">
                                                <p class="single-item-title">{{$spkm->name}}</p>
                                                <p class="single-item-price">
                                                @if($spkm->promotion_price==0)
                                                        <span>{{$spkm->unit_price}},000vnđ</span>
                                                    @else
                                                        <span class="flash-del">{{$spkm->unit_price}},000vnđ</span>
                                                        <span class="flash-sale">{{$spkm->promotion_price}},000vnđ</span>
                                                    @endif
                                                </p>
                                            </div>
                                            <div class="single-item-caption">
                                                <a class="add-to-cart pull-left" href="shopping_cart.html"><i class="fa fa-shopping-cart"></i></a>
                                                <a class="beta-btn primary" href="product.html">Chi tiết <i class="fa fa-chevron-right"></i></a>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>

                        </div> <!-- .beta-products-list -->
                    </div>
                </div> <!-- end section with sidebar and main content -->


            </div> <!-- .main-content -->
        </div> <!-- #content -->
    </div>
@endsection