@extends('site.master')
@section('title','Giỏ hàng')
@section('content')
    <div class="breadcrumbs">
        <div class="container">
            <ol class="breadcrumb breadcrumb1 animated wow slideInLeft animated" data-wow-delay=".5s" style="visibility: visible; animation-delay: 0.5s; animation-name: slideInLeft;">
                <li><a href="/"><span class="glyphicon glyphicon-home" aria-hidden="true"></span>Home</a></li>
                <li class="active">Giỏ hàng</li>
            </ol>
        </div>
    </div>
    <!---->
    <div class="container">
        <div class="check-out">
            <h2>Giỏ hàng</h2>
            @if(Session::has('cart'))
                @if(!empty($product_cart))
            <table >
                <tr>
                    <th>Sản phẩm</th>
                    <th>Số lượng</th>
                    <th>Giá</th>
                    <th>Thành tiền</th>
                    <th>Hành động</th>
                </tr>

                @foreach($product_cart as $item)
                <tr>
                    <td class="ring-in"><a href="single.html" class="at-in"><img src="uploads/product/{{$item['item']['image']}}" class="img-responsive" alt=""></a>
                        <div class="sed">
                            <h5>{{$item['item']['name']}}</h5>
                        </div>
                        <div class="clearfix"> </div></td>
                    <td class="check"><input type="text" value="1" product_id="{{$item['item']['id']}}" class="product_qtv"></td>
                    <td>
                        @if($item['item']['promotion_price']>0)
                            <h5 class="item_price">{{number_format($item['item']['promotion_price'],3)}} vnđ</h5>

                        @else
                            <h5 class="item_price">{{number_format($item['item']['unit_price'],3)}} vnđ</h5>
                        @endif</td>
                    <td class="txt_thanhtien">  @if($item['qty'] *$item['item']['promotion_price']==0)
                            {{number_format($item['qty'] *$item['item']['unit_price'],3)}} vnđ
                        @else
                            {{number_format($item['qty'] *$item['item']['promotion_price'],3)}} vnđ
                        @endif</td>
                    <td><a href="{{route('xoagiohang',$item['item']['id'])}}"><button class="btn btn-warning">Xóa</button></a></td>
                </tr>
                    @endforeach

            </table>
            <h3>Tổng tiền: <span class="txt_tongtien">{{number_format(Session('cart')->totalPrice,3)}} vnđ</span></h3>

            <a href="{{route('checkout')}}" class=" to-buy">Tiếp tục</a>
                @endif
            @endif
            <div class="clearfix"> </div>
        </div>
    </div>
@endsection