@if(Session::has('mailmessage'))
    <div class="alert alert-danger">
        {{Session::get('mailmessage')}}
    </div>
@endif
@extends('site.master')
@section('title','trang chủ')
@section('content')

    <div class="content">
        <div class="container">
            <div class="content-top">
                <div class="content-top1">
                    @foreach($topsale_left as $item)
                    <div class="col-md-3 col-md2 animated wow fadeInLeft" data-wow-delay=".5s">
                        <span class="sale_pro1"><img src="uploads/cate/sale.png" width="50px" alt=""></span>
                        <div class="col-md1 simpleCart_shelfItem">
                            <a href="{{route('chitietsanpham',['id'=>$item->id,'slug'=>$item->alias])}}">
                                <img class="img-responsive img-loaisp" src="uploads/product/{{$item->image}}" alt="" />
                            </a>
                            <p><a href="{{route('chitietsanpham',['id'=>$item->id,'slug'=>$item->alias])}}">{{$item->name}}</a></p>
                            <div class="price">
                                @if($item->promotion_price>0)
                                    <h5 class="item_price">{{number_format($item->promotion_price,3)}} vnđ <span><sup><del style="color: red">{{number_format($item->unit_price,3)}} vnđ </del></sup></span></h5>

                                @else
                                    <h5 class="item_price">{{number_format($item->unit_price,3)}} vnđ</h5>
                                @endif
                                <a href="{{route('themgiohang',['id'=>$item->id])}}" class="item_add">Mua hàng</a>
                                <div class="clearfix"> </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                    <div class="col-md-6 animated wow fadeInDown animated" data-wow-delay=".5s" style="visibility: visible; animation-delay: 0.5s; animation-name: fadeInDown;">
                        <div class="col-md3">
                            <div class="up-t">
                                <h3>khuyến mãi tới 50% / 1 tuần</h3>
                            </div>
                        </div>
                    </div>
                        @foreach($topsale_right as $item)
                    <div class="col-md-3 col-md2 animated wow fadeInRight" data-wow-delay=".5s">
                        <span class="sale_pro1"><img src="uploads/cate/sale.png" width="50px" alt=""></span>
                        <div class="col-md1 simpleCart_shelfItem">
                            <a href="{{route('chitietsanpham',['id'=>$item->id,'slug'=>$item->alias])}}">
                                <img class="img-responsive img-loaisp" src="uploads/product/{{$item->image}}" alt="" />
                            </a>
                            <p><a href="{{route('chitietsanpham',['id'=>$item->id,'slug'=>$item->alias])}}">{{strtolower($item->name)}}</a></p>
                            <div class="price">
                               @if($item->promotion_price>0)
                                <h5 class="item_price">{{number_format($item->promotion_price,3)}} vnđ <span><sup><del style="color: red">{{number_format($item->unit_price,3)}} vnđ </del></sup></span></h5>

                            @else
                                <h5 class="item_price">{{number_format($item->unit_price,3)}} vnđ</h5>

                            @endif
                                <a href="{{route('themgiohang',['id'=>$item->id])}}" class="item_add">Mua hàng</a>
                                <div class="clearfix"> </div>
                            </div>
                        </div>
                    </div>
                        @endforeach
                    <div class="clearfix"> </div>
                </div>

                {{--sản phẩm mới--}}

                <div class="content-top1">
                    @foreach($new_product as $item)
                    <div class="col-md-3 col-md2 animated wow fadeInLeft" data-wow-delay=".5s">
                        <span class="sale_pro1"><img src="uploads/cate/new.png" width="50px" alt=""></span>
                        <div class="col-md1 simpleCart_shelfItem swap-loai1">
                            <a href="{{route('chitietsanpham',['id'=>$item->id,'slug'=>$item->alias])}}">
                                <img class="img-responsive img-loaisp" src="uploads/product/{{$item->image}}" alt="" width="250px"/>
                            </a>
                            <p><a href="{{route('chitietsanpham',['id'=>$item->id,'slug'=>$item->alias])}}">{{$item->name}}</a></p>
                            <div class="price">
                                @if($item->promotion_price>0)
                                    <h5 class="item_price">{{number_format($item->promotion_price,3)}} vnđ <span><sup><del style="color: red">{{number_format($item->unit_price,3)}} vnđ </del></sup></span></h5>

                                @else
                                    <h5 class="item_price">{{number_format($item->unit_price,3)}} vnđ</h5>

                                @endif
                                <a href="{{route('themgiohang',['id'=>$item->id])}}" class="item_add">Mua hàng</a>
                                <div class="clearfix"> </div>
                            </div>
                        </div>
                    </div>
@endforeach
                    <div class="clearfix"> </div>
                </div>
            </div>
        </div>
    </div>
    <!--//content-->

    <div class="con-tp">
        <div class="container">
            <div class="col-md-4 con-tp-lft animated wow fadeInLeft" data-wow-delay=".5s">
                <a href="products.html">
                    <div class="content-grid-effect slow-zoom vertical">
                        <div class="img-box"><img src="site/images/6.jpg" alt="image" class="img-responsive zoom-img"></div>
                        <div class="info-box">
                            <div class="info-content simpleCart_shelfItem">
                                {{--<h4>30%offer</h4>--}}
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-4 con-tp-lft animated wow fadeInDown animated" data-wow-delay=".5s" style="visibility: visible; animation-delay: 0.5s; animation-name: fadeInDown;">
                <a href="products.html">
                    <div class="content-grid-effect slow-zoom vertical">
                        <div class="img-box"><img src="site/images/10.jpg" alt="image" class="img-responsive zoom-img"></div>
                        <div class="info-box">
                            <div class="info-content simpleCart_shelfItem">
                                {{--<h4>45%offer</h4>--}}
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-4 con-tp-lft animated wow fadeInRight" data-wow-delay=".5s">
                <a href="products.html">
                    <div class="content-grid-effect slow-zoom vertical">
                        <div class="img-box"><img src="site/images/9.jpg" alt="image" class="img-responsive zoom-img"></div>
                        <div class="info-box">
                            <div class="info-content simpleCart_shelfItem">
                                {{--<h4>50%offer</h4>--}}
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-4 con-tp-lft animated wow fadeInLeft" data-wow-delay=".5s">
                <a href="products.html">
                    <div class="content-grid-effect slow-zoom vertical">
                        <div class="img-box"><img src="site/images/12.jpg" alt="image" class="img-responsive zoom-img"></div>
                        <div class="info-box">
                            <div class="info-content simpleCart_shelfItem">
                                {{--<h4>25%offer</h4>--}}
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-4 con-tp-lft animated wow fadeInUp animated" data-wow-delay=".5s" style="visibility: visible; animation-delay: 0.5s; animation-name: fadeInUp;">
                <a href="products.html">
                    <div class="content-grid-effect slow-zoom vertical">
                        <div class="img-box"><img src="site/images/13.jpg" alt="image" class="img-responsive zoom-img"></div>
                        <div class="info-box">
                            <div class="info-content simpleCart_shelfItem">
                                {{--<h4>50%offer</h4>--}}
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-4 con-tp-lft animated wow fadeInRight" data-wow-delay=".5s">
                <a href="products.html">
                    <div class="content-grid-effect slow-zoom vertical">
                        <div class="img-box"><img src="site/images/14.jpg" alt="image" class="img-responsive zoom-img"></div>
                        <div class="info-box">
                            <div class="info-content simpleCart_shelfItem">
                                {{--<h4>35%offer</h4>--}}
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>

    <div class="c-btm">
        <div class="content-top1">
            <div class="container">
                @foreach($sale_product as $item)
                    <div class="col-md-3 col-md2 animated wow fadeInLeft" data-wow-delay=".5s">
                        <span class="sale_pro1"><img src="uploads/cate/sale.png" width="50px" alt=""></span>
                        <div class="col-md1 simpleCart_shelfItem swap-loai">
                            <a href="{{route('chitietsanpham',['id'=>$item->id,'slug'=>$item->alias])}}">
                                <img class="img-responsive img-loaisp" src="uploads/product/{{$item->image}}" alt="" width="250px"/>
                            </a>
                            <p><a href="{{route('chitietsanpham',['id'=>$item->id,'slug'=>$item->alias])}}">{{strtolower($item->name)}}</a></p>
                            <div class="price">
                                @if($item->promotion_price>0)
                                    <h5 class="item_price">{{number_format($item->promotion_price,3)}} vnđ <span><sup><del style="color: red">{{number_format($item->unit_price,3)}} vnđ </del></sup></span></h5>

                                @else
                                    <h5 class="item_price">{{number_format($item->unit_price,3)}} vnđ</h5>

                                @endif
                                <a href="{{route('themgiohang',['id'=>$item->id])}}" class="item_add">Mua hàng</a>
                                <div class="clearfix"> </div>
                            </div>
                        </div>
                    </div>
                @endforeach
                <div class="clearfix"> </div>
            </div>
        </div>
    </div>

@endsection