@extends('site.master')
@section('title','đặt hàng')
@section('content')
    <div class="breadcrumbs">
        <div class="container">
            <ol class="breadcrumb breadcrumb1 animated wow slideInLeft animated" data-wow-delay=".5s" style="visibility: visible; animation-delay: 0.5s; animation-name: slideInLeft;">
                <li><a href="/"><span class="glyphicon glyphicon-home" aria-hidden="true"></span>Trang chủ</a></li>
                <li class="active">Đặt hàng</li>
            </ol>
        </div>
    </div>

    <div class="container">
        @if(Session::has('thong bao'))
            <div class="errors-login">
                <h3>{{Session::get('thong bao')}}</h3>
            </div>
        @endif
        <div class="register">
            <form action="{{route('checkout')}}" method="post" id="thongtin_form">
            <div class="col-md-6 ">
                <h3 class="per">Thông tin cá nhân</h3>
                    {{  csrf_field()}}
                <div class="form-group">
                    <label for="">Họ và tên:</label>
                    <input class="form-control" type="text" id="name" name="name" value="{{isset($data)?$data->name:" "}}">
                </div>
                <div class="form-group">
                    <label for="">Giới tính</label>
                    @if(isset($data) && isset($data->gender)=="nam")

                        <input id="gender" type="radio" class="input-radio" name="gender" value="nam" checked="checked" style="width: 10%"><span style="margin-right: 10%">Nam</span>
                        <input id="gender" type="radio" class="input-radio" name="gender" value="nữ" style="width: 10%"><span>Nữ</span>
                    @elseif(isset($data) && isset($data->gender)=="nữ")
                        <input id="gender" type="radio" class="input-radio" name="gender" value="nam"  style="width: 10%"><span style="margin-right: 10%">Nam</span>
                        <input id="gender" type="radio" class="input-radio" name="gender" value="nữ" checked="checked" style="width: 10%"><span>Nữ</span>
                    @else
                        <input id="gender" type="radio" class="input-radio" name="gender" value="nam" checked="checked" style="width: 10%"><span style="margin-right: 10%">Nam</span>
                        <input id="gender" type="radio" class="input-radio" name="gender" value="nữ" style="width: 10%"><span>Nữ</span>
                    @endif
                </div>
                <div class="form-group">
                    <label for="">Email:</label>
                    <input class="form-control" type="email" id="email" name="email"    value="{{isset($data)?$data->email:" "}}">
                </div>
                <div class="form-group">
                    <label for="">Địa chỉ:</label>
                    <input class="form-control" type="text" id="adress" name="adress"  value="{{isset($data)?$data->address:" "}}"  required>
                </div>
                <div class="form-group">
                    <label for="">SDT:</label>
                    <input class="form-control" type="text" id="phone" name="phone"  value="{{isset($data)?$data->phone_number:" "}}" required>
                </div>
                <div class="form-group">
                    <label for="">Ghi chú:</label>
                    <textarea class="form-control" id="notes" name="note" ></textarea>
                </div>
            </div>
            <div class=" col-md-6">
                <h3>Thông tin đơn hàng</h3>
                <div class="your-order">
                    @if(Session::has('cart'))
                        <div class="your-order-body" style="padding: 0px 10px">
                            <div class="your-order-item">
                                <div>
                                    <!--  one item	 -->
                                    @foreach($product_cart as $product)
                                        <div class="media">
                                            <img width="15%" src="uploads/product/{{$product['item']['image']}}" alt="" class="pull-left">
                                            <div class="media-body">
                                                <p class="font-large">{{$product['item']['name']}}</p>
                                             <span class="color-gray your-order-info">  Số lượng: {{$product['qty']}}</span>
                                            </div>
                                        </div>
                                @endforeach
                                <!-- end one item -->
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="your-order-item">
                                <div class="pull-left"><h3 class="your-order-f18">Tổng tiền:</h3></div>
                                <div class="pull-right"><h5 class="color-black">{{number_format(Session('cart')->totalPrice,3,',','.')}}vnđ</h5></div>
                                <div class="clearfix"></div>
                            </div>
                        </div>

                    <div class="your-order-head"><h3>Hình thức thanh toán</h3></div>

                    <div class="your-order-body">
                        <ul class="payment_methods methods">
                            <li class="payment_method_bacs">
                                <input id="payment_method_bacs" type="radio" class="input-radio" name="payment_method" value="COD" checked="checked" data-order_button_text="">
                                <label for="payment_method_bacs">Thanh toán khi nhận hàng </label>
                                <div class="payment_box payment_method_bacs" style="display: block;">
                                    Cửa hàng sẽ gửi hàng đến địa chỉ của bạn, bạn xem hàng rồi thanh toán tiền cho nhân viên giao hàng
                                </div>
                            </li>

                            <li class="payment_method_cheque">
                                <input id="payment_method_cheque" type="radio" class="input-radio" name="payment_method" value="ATM" data-order_button_text="">
                                <label for="payment_method_cheque">Chuyển khoản </label>
                                <div class="payment_box payment_method_cheque" style="display: none;">
                                    Chuyển tiền đến tài khoản sau:
                                    <br>- Số tài khoản:711A34527836
                                    <br>- Chủ TK: Nguyễn Hảo
                                    <br>- Ngân hàng VietinBank, Chi nhánh Thái Nguyên
                                    <br>- Mọi thắc mắc xin liên hệ:096534212
                                </div>
                            </li>

                        </ul>
                    </div>
                    @endif
                </div> <!-- .your-order -->
            </div>
                <div class="clearfix"> </div>
                <div class="register-but">
                    <input type="submit" value="Đặt hàng"></div>
                <div class="clearfix"> </div>
            </form>
        </div>
    </div>
    @endsection