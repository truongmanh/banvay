@extends('site.master')
@section('content')
    <div class="breadcrumbs">
        <div class="container">
            <ol class="breadcrumb breadcrumb1 animated wow slideInLeft animated" data-wow-delay=".5s" style="visibility: visible; animation-delay: 0.5s; animation-name: slideInLeft;">
                <li><a href="/"><span class="glyphicon glyphicon-home" aria-hidden="true"></span>Trang chủ</a></li>
                <li class="active">Đăng ký</li>
            </ol>
        </div>
    </div>
    <div class="container">
        <div class="register">
            <h2>Đăng ký / Đăng nhập</h2>
            @if(Session::has('thong bao'))
                <div class="errors-login">
                    <h5>{{Session::get('thong bao')}}</h5>
                </div>
            @endif
                <div class="col-md-6  register-top-grid">
                    <h3>Đăng ký</h3>
                    <form action="{{route('dangky')}}" method="post" id="dangky_form">
                      {{  csrf_field()}}
                    <div class="mation">
                        <span>Tên đăng nhâp</span>
                        <input type="text" name="txt_name">

                        <span>Email</span>
                        <input type="email" name="txt_email">

                        <span>Mật khẩu</span>
                        <input type="password" name="txt_password">
                    </div>
                    <div class="clearfix"> </div>
                        <div class="register-but">
                         <input type="submit" value="Đăng ký"></div>
                        <div class="clearfix"> </div>
                    </form>
                </div>

                <div class=" col-md-6 register-bottom-grid">
                    <h3>Đăng nhập</h3>
                    <form action="{{route('dangnhap')}}" method="post" id="dangnhap_form">
                        {{  csrf_field()}}
                    <div class="mation">
                        <span>Tên đăng nhập</span>
                        <input type="text" name="txt_name">
                        <span>Password</span>
                        <input type="password" name="txt_password">
                    </div>
                        <div class="clearfix"> </div>
                        <div class="register-but">
                            <input type="submit" value="Đăng nhập">
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
    @endsection()