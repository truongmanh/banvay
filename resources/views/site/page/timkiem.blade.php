@extends('site.master')
@section('title','Tìm kiếm')
@section('content')
    <div class="products">
        <div class="container">
            <h2>Kết quả tìm kiếm</h2>
            <div class="col-md-9">

                <div class="content-top1">
                    @foreach($product as $item)
                        <div class="col-md-4 col-md4">
                            <div class="col-md1 simpleCart_shelfItem swap-loai">
                                <a href="{{route('chitietsanpham',['id'=>$item->id,'slug'=>$item->alias])}}">
                                    <img class="img-responsive img-loaisp" src="uploads/product/{{$item->image}}" alt="" />
                                </a>
                                <p><a href="{{route('chitietsanpham',['id'=>$item->id,'slug'=>$item->alias])}}">{{$item->name}}</a></p>
                                <div class="price">
                                    @if($item->promotion_price>0)
                                        <h5 class="item_price">{{number_format($item->promotion_price,3)}} vnđ <span><sup><del style="color: red">{{number_format($item->unit_price,3)}} vnđ </del></sup></span></h5>
                                    @else
                                        <h5 class="item_price">{{number_format($item->unit_price,3)}} vnđ</h5>
                                    @endif
                                    <a href="#" class="item_add">Mua hàng</a>
                                    <div class="clearfix"> </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    <div class="clearfix"> </div>
                </div>

            </div>
            <div class="col-md-3 product-bottom">
                <!--categories-->
                <div class=" rsidebar span_1_of_left">
                    <h3 class="cate">Danh mục</h3>
                    <ul class="menu-drop">
                        @foreach($loai as $item)
                            <li ><a href="{{route('loaisanpham',['id'=>$item->id,'slug'=>$item->alias])}}">{{$item->name}}</a>
                            </li>
                        @endforeach
                    </ul>
                </div>
                <!--//menu-->
                <!--seller-->
                <div class="product-bottom">
                    <h3 class="cate">Sản phẩm bán chạy</h3>
                    @foreach($pro_hot as $item)
                        <div class="product-go">
                            <div class=" fashion-grid">
                                <a href="{{route('chitietsanpham',['id'=>$item->id,'slug'=>$item->alias])}}"><img class="img-responsive " src="uploads/product/{{$item->image}}" alt=""></a>
                            </div>
                            <div class=" fashion-grid1">
                                <h6 class="best2"><a href="{{route('chitietsanpham',['id'=>$item->id,'slug'=>$item->alias])}}" >{{$item->name}}</a></h6>
                                <span class=" price-in1">   @if($item->promotion_price>0)
                                        <h5 class="item_price">{{number_format($item->promotion_price,3)}} vnđ <span><sup><del style="color: red">{{number_format($item->unit_price,3)}} vnđ </del></sup></span></h5>
                                    @else
                                        <h5 class="item_price">{{number_format($item->unit_price,3)}} vnđ</h5>
                                    @endif</span>
                            </div>
                            <div class="clearfix"> </div>
                        </div>
                    @endforeach
                </div>

                <!--//seller-->
                <!--tag-->
                <div class="tag">
                    <h3 class="cate">Tags</h3>
                    <div class="tags">
                        <ul>
                            <li><a href="#">Đầm xòe</a></li>
                            <li><a href="#">Thời trang</a></li>
                            <li><a href="#">công sở</a></li>
                            <li><a href="#">Đầm sơ mi</a></li>
                            <li><a href="#">váy nữ</a></li>
                            <li><a href="#">váy</a></li>

                            <div class="clearfix"> </div>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>
@endsection
