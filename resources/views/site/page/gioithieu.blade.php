@extends('site.master')
@section('title','Giới-thiệu')
@section('content')
    <div class="container">
        <div id="content" class="space-top-none">

            <div class="space50">&nbsp;</div>
            <div class="row">
                <h2>CÔNG TY TNHH THƯƠNG MẠI LD TRUNG VIỆT</h2>
                <p>Địa chỉ: Tổ 28, P.Phan Đình Phùng, TP.Thái Nguyên</p>
                <p>	Điện thoại: 0868 603 832</p>
                <p>	Email: huachicuong90@gmail.com</p>
                <p>	Website: http://ldtrungviet.com</p>
            </div>
        </div> <!-- #content -->
    </div> <!-- .container -->
@endsection