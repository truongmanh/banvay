@extends('site.master')
@section('title','liên hệ')
@section('content')
    <div class="breadcrumbs">
        <div class="container">
            <ol class="breadcrumb breadcrumb1 animated wow slideInLeft animated" data-wow-delay=".5s" style="visibility: visible; animation-delay: 0.5s; animation-name: slideInLeft;">
                <li><a href="/"><span class="glyphicon glyphicon-home" aria-hidden="true"></span>Trang chủ</a></li>
                <li class="active">Liên hệ</li>
            </ol>
        </div>
    </div>
    <div class="container">
        <div id="content" class="space-top-none">

            <div class="space50">&nbsp;</div>
            <div class="row">
                <div class="col-sm-8">
                    <h2>Liên hệ</h2>
                    <div class="space20">&nbsp;</div>
                    <p>Mọi thắc mắc vui lòng bạn gửi mail cho chúng tôi</p>
                    <div class="space20">&nbsp;</div>
                    <form action="{{route('lienhe')}}" method="post" class="contact-form" id="formlh">
                        {{csrf_field()}}
                        <div class="form-group">
                            <input class="form-control" name="email" type="text" placeholder="email">
                        </div>
                        <div class="form-group">
                            <input class="form-control" name="subject" type="text" placeholder="chủ đề">
                        </div>
                        <div class="form-group">
                            <textarea class="form-control" name="message" placeholder="nội dung"></textarea>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="beta-btn primary">Gửi tin nhắn<i class="fa fa-chevron-right"></i></button>
                        </div>
                    </form>
                </div>
                <div class="col-sm-4">
                    <h2>Thông tin liên hệ</h2>

                    <div class="space20">&nbsp;</div>

                    <h6 class="contact-title">Địa chỉ:</h6>
                    <p> Tổ 28, Phường Tân Thịnh, TP.Thái Nguyên
                    </p>
                    <div class="space20">&nbsp;</div>
                    <h6 class="contact-title">Điện thoại:</h6>
                    <p> 0968 984 283
                    </p>
                    <div class="space20">&nbsp;</div>
                </div>
            </div>
        </div> <!-- #content -->
    </div> <!-- .container -->
@endsection