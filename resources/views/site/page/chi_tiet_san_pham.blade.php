@extends('site.master')
@section('title','Chi tiết sản phẩm')
@section('content')
    <div class="breadcrumbs">
        <div class="container">
            <ol class="breadcrumb breadcrumb1 animated wow slideInLeft animated" data-wow-delay=".5s" style="visibility: visible; animation-delay: 0.5s; animation-name: slideInLeft;">
                <li><a href="/"><span class="glyphicon glyphicon-home" aria-hidden="true"></span>Trang chủ</a></li>
                <li class="active">Chi tiết sản phẩm</li>
            </ol>
        </div>
    </div>
    <div class="single">

        <div class="container">
            <div class="col-md-9">
                {{--@foreach($detail as $item)--}}
                <div class="col-md-5 grid">
                    <div class="flexslider">
                        <ul class="slides">
                            <li data-thumb="uploads/product/{{$detail->image}}">
                                <div class="thumb-image"> <img src="uploads/product/{{$detail->image}}" data-imagezoom="true" class="img-responsive"> </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-7 single-top-in">
                    <div class="single-para simpleCart_shelfItem">
                        <h2>{{$detail->name}}</h2>
                        <p>{!!$detail->description!!}</p>
                        <label for="">Đánh giá:</label>
                        <div class="star-on">

                            <ul>
                                <li><a href="#"><i class="glyphicon glyphicon-star"> </i></a></li>
                                <li><a href="#"><i class="glyphicon glyphicon-star"> </i></a></li>
                                <li><a href="#"><i class="glyphicon glyphicon-star"> </i></a></li>
                                <li><a href="#"><i class="glyphicon glyphicon-star"> </i></a></li>
                                <li><a href="#"><i class="glyphicon glyphicon-star"> </i></a></li>
                            </ul>
                            <div class="clearfix"> </div>
                        </div>

                        <label  class="add-to item_price">
                            @if($detail->promotion_price>0)
                                <h3 class="item_price">Giá:{{number_format($detail->promotion_price,3)}} vnđ <span><sup><del style="color: red">{{number_format($detail->unit_price,3)}} vnđ </del></sup></span></h3>

                            @else
                                <h3 class="item_price">Giá:{{number_format($detail->unit_price,3)}} vnđ</h3>
                            @endif
                        </label>

                        <a href="#" class="cart item_add">Mua hàng</a>
                    </div>
                </div>
                {{--@endforeach--}}
                <hr>
                <div class="clearfix"> </div>
                <div class="content-top1">
                    @foreach($spsame as $item)
                    <div class="col-md-4 col-md4">
                        <div class="col-md1 simpleCart_shelfItem">
                            <a href="{{route('chitietsanpham',['id'=>$item->id,'slug'=>$item->alias])}}">
                                <img class="img-responsive img-loaisp" src="uploads/product/{{$item->image}}" alt="" />
                            </a>
                            <p><a href="{{route('chitietsanpham',['id'=>$item->id,'slug'=>$item->alias])}}">{{$item->name}}</a></p>
                            <div class="price">
                                @if($detail->promotion_price>0)
                                    <h5 class="item_price">Giá:{{number_format($detail->promotion_price,3)}} vnđ <span><sup><del style="color: red">{{number_format($detail->unit_price,3)}} vnđ </del></sup></span></h5>

                                @else
                                    <h3 class="item_price">Giá:{{number_format($detail->unit_price,3)}} vnđ</h3>
                                @endif
                                <a href="#" class="item_add">Mua hàng</a>
                                <div class="clearfix"> </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                    <div class="clearfix"> </div>
                </div>
            </div>
            <!----->
            <div class="col-md-3 product-bottom">
                <!--categories-->
                <div class=" rsidebar span_1_of_left">
                    <h3 class="cate">Danh mục</h3>
                    <ul class="menu-drop">
                        @foreach($loai as $item)
                            <li ><a href="{{route('loaisanpham',['id'=>$item->id,'slug'=>$item->alias])}}">{{$item->name}}</a>
                            </li>
                        @endforeach
                    </ul>
                </div>
                <!--//menu-->
                <!--seller-->
                <div class="product-bottom">
                    <h3 class="cate">Sản phẩm bán chạy</h3>
                    @foreach($pro_hot as $item)
                        <div class="product-go">
                            <div class=" fashion-grid">
                                <a href="{{route('chitietsanpham',['id'=>$item->id,'slug'=>$item->alias])}}"><img class="img-responsive " src="uploads/product/{{$item->image}}" alt=""></a>
                            </div>
                            <div class=" fashion-grid1">
                                <h6 class="best2"><a href="{{route('chitietsanpham',['id'=>$item->id,'slug'=>$item->alias])}}" >{{$item->name}}</a></h6>
                                <span class=" price-in1">   @if($item->promotion_price>0)
                                        <h5 class="item_price">{{number_format($item->promotion_price,3)}} vnđ <span><sup><del style="color: red">{{number_format($item->unit_price,3)}} vnđ </del></sup></span></h5>
                                    @else
                                        <h5 class="item_price">{{number_format($item->unit_price,3)}} vnđ</h5>
                                    @endif</span>
                            </div>
                            <div class="clearfix"> </div>
                        </div>
                    @endforeach
                </div>

                <!--//seller-->
                <!--tag-->
                <div class="tag">
                    <h3 class="cate">Tags</h3>
                    <div class="tags">
                        <ul>
                            <li><a href="#">Đầm xòe</a></li>
                            <li><a href="#">Thời trang</a></li>
                            <li><a href="#">công sở</a></li>
                            <li><a href="#">Đầm sơ mi</a></li>
                            <li><a href="#">váy nữ</a></li>
                            <li><a href="#">váy</a></li>

                            <div class="clearfix"> </div>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>
    @endsection