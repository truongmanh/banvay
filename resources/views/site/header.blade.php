<div class="header">
    <div class="header-top">
        <div class="container">
            <div class="col-sm-4 logo animated wow fadeInLeft" data-wow-delay=".5s">
                <h1><a href="/">Shop<span>vayxinh</span></a></h1>
            </div>
            <div class="col-sm-4 world animated wow fadeInRight" data-wow-delay=".5s">
                <div class="cart box_1">
                    <a href="{{route('giohang')}}">
                        {{--<h3> <div class="total">--}}
                                {{--<span class="simpleCart_total"></span></div>--}}
                            <img src="site/images/cart.png" alt=""/>

                    </a>
                    <p><a href="{{route('giohang')}}">Giỏ hàng
                            @if(Session::has('cart')) {{"(".Session('cart')->totalQty.")"}}
                            @else (trống)
                            @endif</a></p>

                </div>
            </div>
            <div class="col-sm-2 number animated wow fadeInRight" data-wow-delay=".5s">
                <span><i class="glyphicon glyphicon-phone"></i>085 596 234</span>
                <p>Liên hệ</p>
            </div>
            <div class="col-sm-2 search animated wow fadeInRight" data-wow-delay=".5s">
                <a class="play-icon popup-with-zoom-anim" href="#small-dialog"><i class="glyphicon glyphicon-search"> </i> </a>
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>
    <div class="container">
        <div class="head-top">
            <div class="n-avigation">

                <nav class="navbar nav_bottom" role="navigation">

                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header nav_2">
                        <button type="button" class="navbar-toggle collapsed navbar-toggle1" data-toggle="collapse" data-target="#bs-megadropdown-tabs">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="#"></a>
                    </div>
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-megadropdown-tabs">
                        <ul class="nav navbar-nav nav_1">
                            <li><a href="/">Trang chủ</a></li>
                            {{--<li class="dropdown mega-dropdown active">--}}
                                {{--<a href="#" class="dropdown-toggle" data-toggle="dropdown">Women<span class="caret"></span></a>--}}
                                {{--<div class="dropdown-menu mega-dropdown-menu">--}}
                                    {{--<div class="container-fluid">--}}
                                        {{--<!-- Tab panes -->--}}
                                        {{--<div class="tab-content">--}}
                                            {{--<div class="tab-pane active" id="men">--}}
                                                {{--<ul class="nav-list list-inline">--}}
                                                    {{--<li><a href="women.html"><img src="site/images/t7.jpg" class="img-responsive" alt=""/></a></li>--}}
                                                    {{--<li><a href="women.html"><img src="site/images/t8.jpg" class="img-responsive" alt=""/></a></li>--}}
                                                    {{--<li><a href="women.html"><img src="site/images/t9.jpg" class="img-responsive" alt=""/></a></li>--}}
                                                    {{--<li><a href="women.html"><img src="site/images/t11.jpg" class="img-responsive" alt=""/></a></li>--}}
                                                    {{--<li><a href="women.html"><img src="site/images/t1.jpg" class="img-responsive" alt=""/></a></li>--}}
                                                    {{--<li><a href="women.html"><img src="site/images/t12.jpg" class="img-responsive" alt=""/></a></li>--}}
                                                {{--</ul>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                    {{--<!-- Nav tabs -->--}}

                                {{--</div>--}}
                            {{--</li>--}}
                            @foreach($loaisp as $item)
                            <li><a href="{{route('loaisanpham',['id'=>$item->id,'slug'=>$item->alias])}}">{{$item->name}}</a></li>
                            @endforeach

                            <li ><a href="{{route('lien-he')}}">Liên hệ</a></li>
                           @if(Auth::check())
                                <li ><a href="#">xin chào: {{Auth::user()->name}}</a></li>
                            <li ><a href="{{route('dangxuat')}}">Đăng xuất</a></li>
                               @else
                                <li ><a href="{{route('dangky')}}">Đăng nhập/Đăng ký</a></li>
                               @endif
                        </ul>
                    </div><!-- /.navbar-collapse -->

                </nav>
            </div>


            <div class="clearfix"> </div>
            <!---pop-up-box---->
            <link href="site/css/popuo-box.css" rel="stylesheet" type="text/css" media="all"/>
            <script src="site/js/jquery.magnific-popup.js" type="text/javascript"></script>
            <!---//pop-up-box---->
            <div id="small-dialog" class="mfp-hide">
                <div class="search-top">
                    <div class="login">
                        <form action="{{route('Search-Product')}}" method="post">
                            {{csrf_field()}}
                            <input type="submit" value="">
                            <input type="text" name="search" value="Type something..." onfocus="this.value = '';" onblur="if (this.value == '') {this.value = '';}">

                        </form>
                    </div>
                    <p>	Shopping</p>
                </div>
            </div>
            <script>
                $(document).ready(function() {
                    $('.popup-with-zoom-anim').magnificPopup({
                        type: 'inline',
                        fixedContentPos: false,
                        fixedBgPos: true,
                        overflowY: 'auto',
                        closeBtnInside: true,
                        preloader: false,
                        midClick: true,
                        removalDelay: 300,
                        mainClass: 'my-mfp-zoom-in'
                    });

                });
            </script>
            <!---->
        </div>
    </div>
</div>