<div class="footer">
    <div class="container">
        <div class="footer-top">
            <div class="col-md-6 top-footer animated wow fadeInLeft" data-wow-delay=".5s">
                <h3>Mạng xã hội</h3>
                <div class="social-icons">
                    <ul class="social">
                        <li><a href="#"><i></i></a> </li>
                        <li><a href="#"><i class="facebook"></i></a></li>
                        <li><a href="#"><i class="google"></i> </a></li>
                        <li><a href="#"><i class="linked"></i> </a></li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
            </div>
            {{--<div class="col-md-6 top-footer1 animated wow fadeInRight" data-wow-delay=".5s">--}}
                {{--<h3>Newsletter</h3>--}}
                {{--<form action="#" method="post">--}}
                    {{--<input type="text" name="email" value="" onfocus="this.value='';" onblur="if (this.value == '') {this.value ='';}">--}}
                    {{--<input type="submit" value="SUBSCRIBE">--}}
                {{--</form>--}}
            {{--</div>--}}
            <div class="clearfix"> </div>
        </div>
    </div>
    <div class="footer-bottom">
        <div class="container">
            <div class="col-md-4 footer-bottom-cate animated wow fadeInLeft" data-wow-delay=".5s">
                <h6>Danh mục</h6>
                <ul>
                    @foreach($loaisp as $item)
                    <li><a href="{{route('loaisanpham',['id'=>$item->id,'slug'=>$item->alias])}}">{{$item->name}}</a></li>
                        @endforeach
                </ul>
            </div>
            <div class="col-md-4 footer-bottom-cate animated wow fadeInLeft" data-wow-delay=".5s">
                <h6>Hỗ trợ khách hàng</h6>
                <ul>
                    <li><a href="#">Góp ý-Phản hồi</a></li>
                    <li><a href="#">Liên hệ</a></li>
                    <li><a href="#">Chính sách bảo mật</a></li>
                    <li><a href="#">Quy chế hoạt động</a></li>
                    <li><a href="#">Hướng dẫn mua hàng</a></li>
                </ul>
            </div>
            <div class="col-md-4 footer-bottom-cate cate-bottom animated wow fadeInRight" data-wow-delay=".5s">
                <h6>Địa chỉ</h6>
                <ul>
                    <li><i class="glyphicon glyphicon-map-marker" aria-hidden="true"></i>Địa chỉ : Tổ 28, Phường Tân Thịnh, TP.Thái Nguyên</li>
                    <li><i class="glyphicon glyphicon-envelope" aria-hidden="true"></i>Email : <a href="">nguyenhao.com</a></li>
                    <li><i class="glyphicon glyphicon-earphone" aria-hidden="true"></i>Sdt: 0968 984 283</li>
                </ul>
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>
</div>